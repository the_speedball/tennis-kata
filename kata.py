POINT_MAP = {0:'0',
             1: '15',
             2: '30',
             3: '40'}


class Game(object):
    def __init__(self):
        self.points = {1: 0, 2: 0}
        self.finished = False

    def add_point(self, player):
        if not self.finished:
            self.points[player] += 1
        self._resolve_game()

    def _resolve_game(self):
        if self.points[1] >= 3 and self.points[1] - self.points[2] >= 2:
            self.finished = True
        elif self.points[2] >= 3 and self.points[2] - self.points[1] >= 2:
            self.finished = True

    def score(self, player=None):
        if not player:
            return '%s - %s' % (POINT_MAP[self.points[1]],
                                POINT_MAP[self.points[2]])
        return POINT_MAP[self.points[player]]
